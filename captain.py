import os
import sys
import subprocess
import shlex
import docker
from cors import *
from sailor import *

from flask import Flask

from werkzeug.contrib.fixers import ProxyFix
app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)

# connect to host docker api
dkclient = docker.DockerClient(base_url='unix://var/run/docker.sock')

errors = 0
exceptions = 0

_CAPTAINDEBUG = getEnvBoolean("CAPTAIN_DEBUG")
_CAPTAINUNSAFE = getEnvBoolean("CAPTAIN_UNSAFE")

def logDebug(message):
    if _CAPTAINDEBUG and len(str(message).strip()) > 0:
        print(message)

@app.route('/', methods=['GET'])
def nothing():
    return "nothing to see here"

@app.route('/health', methods=['GET'])
def health():
    msg = checkcaptain()
    retorno = "0"
    if errors > 0:
        retorno = "1"
    if exceptions > 0:
        retorno = "2"
    return retorno + "\n" + msg

@app.route('/<gitrepo>/<project>/<branch>', methods=['POST'])
#@crossdomain(origin=['http://github.com', 'http://hereditas.net.br'], headers='Content-Type')
def push(gitrepo,project,branch):
    work(gitrepo,project,branch)
    return "200"

@app.route('/<gitrepo>/<project>/<branch>/unsafe', methods=['GET'])
def pushunsafe(gitrepo,project,branch):
    if _CAPTAINUNSAFE:
        work(gitrepo,project,branch)
        return "200"
    else:
        return "404"  

@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

def checkcaptain():
    ret = "" + br()
    ret += checkdocker() + br()
    ret += checkdockersecret("dh_login") + br()
    ret += checkdockersecret("dh_pwd") + br()
    ret += checkdockerhub() + br()
    return ret

def checkdocker():
    ret = "docker:"
    if dkclient is not None:
        ret += okstring()
        ret += " - version: " + dkclient.version()['Version']
    else:
        ret += failstring()
    return ret

def checkdockersecret(secretname):
    ret = "docker secrets:"
    if existsdockersecret(secretname):
        ret += okstring()
    else:
        ret += failstring()
    return ret + " - " + secretname

def checkdockerhub():
    ret = "docker hub: "
    user = readSecret("dh_login")
    pwd = readSecret("dh_pwd")
    try:
        retlogin = dkclient.login(user,pwd)
    except docker.errors.APIError as e:
        ret += failstring() + " " + str(e)
        logDebug(e)
    return ret

def existsdockersecret(secretname):
    try:
        secretdata = dkclient.secrets.get(secretname)
    except docker.errors.NotFound:
        return False
    return True

def readSecret(secretname):
    secret = ""
    if existsdockersecret(secretname):
        file = open('/run/secrets/' + secretname,"r")
        secret = file.read()
    return secret

def work(gitrepo,project,branch):
    # avoid shell injection
    g = shlex.quote(gitrepo)
    p = shlex.quote(project)
    b = shlex.quote(branch)

    # remove prevoious unfinished or existing same name path
    cmd = ['rm','-fdr', p]
    runcommand(cmd)
    getrepo(g,p,b) # clone repo

    t = readVersion() # read the tag number from repo version file

    dockerbuild(g,p,t)
    #dockerpush(g,p,t,v)
    #removeallimages(gitrepo,project)#remove local images

    cleanmess(g,p) # clean the mess
    return 'done'

def getrepo(gitrepo,project,branch):
    repo = 'git@github.com:{}.git'.format(imagename(gitrepo,project,""))
    cmd = ['git', 'clone', '--recursive', repo]
    runcommand(cmd)# clone repo
    os.chdir(project)# go to project path
    cmd = ['git','checkout',branch] # change branch
    runcommand(cmd)
    return readVersion()

def dockerbuild(gitrepo,project,ptag):
    cmd = ['./setup.sh']
    runcommand(cmd)# create virtual env
    cmd = ['./build-prepare.sh']
    runcommand(cmd)# prepare to build
    try:
        dkclient.images.build(path='.', tag=imagename(gitrepo,project,ptag),quiet=False, rm=True)
        dkclient.images.build(path='.', tag=imagename(gitrepo,project,'latest'),quiet=False, rm=True)
    except docker.errors.BuildError as e1:
        logDebug(e1)
    #cmd = ['./build.sh',tag]
    #runcommand(cmd)# build the container

def dockerpush(gitrepo,project,ptag):
    try:
        dkclient.images.push(imagename(gitrepo,project,""),ptag)
        dkclient.images.push(imagename(gitrepo,project,""))
    except docker.errors.APIError as e2:
        logDebug(e2)
    #cmd = ['./push.sh']
    #runcommand(cmd)# push container to hub

def cleanmess(gitrepo,project):
    os.chdir(os.pardir)# leave project path
    cmd = ['rm','-fdr', project]
    runcommand(cmd)# remove files


def removeallimages(gitrepo,project):
    try:
        imagens = dkclient.images.list(imagename(gitrepo,project,""))
        if len(imagens) > 0:
            for imagem in imagens:
                for tag in imagem.tags:
                    dkclient.images.remove(tag,True,False)
    except docker.errors.APIError as e1:
        logDebug(str(e1))

def runcommand(cmdNargs):
    global errors
    global exceptions
    try:
        result = subprocess.run(cmdNargs, stdout=subprocess.PIPE)
        if result.returncode != 0:
            errors += 1
            logDebug('Erro ======================================================>\n')
            if not result.stderr is None:
                logDebug(result.stderr.decode("utf-8"))
        if not result.stdout is None:
            logDebug(result.stdout.decode("utf-8"))
    except subprocess.CalledProcessError as ex:
        exceptions += 1
        logDebug('Exception ======================================================>\n')
        sys.stdout.write(ex.output)
