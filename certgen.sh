#!/bin/bash

# Copyright (C) 2015-2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

rm sshcaptain
rm sshcaptain.pub
ssh-keygen -t rsa -b 4096 -f sshcaptain -C "admin@hereditas.net.br"
cat sshcaptain.pub
