FROM python:3.6.3-slim-stretch
EXPOSE 8000

# install aditional tool
RUN apt-get update
RUN apt-get install -y git curl openssh-client

# generate keys
COPY sshcaptain /etc/ssh
COPY sshcaptain.pub /etc/ssh
RUN echo "    IdentityFile /etc/ssh/sshcaptain" >> /etc/ssh/ssh_config
RUN echo "    StrictHostKeyChecking=no" >> /etc/ssh/ssh_config
RUN echo "    AddKeysToAgent=yes" >> /etc/ssh/ssh_config
RUN echo "    GlobalKnownHostsFile /etc/ssh/ssh_know_hosts" >> /etc/ssh/ssh_config

# captain
RUN mkdir captain
WORKDIR /usr/src/captain
COPY requirements requirements
COPY setup.sh setup.sh
# prepare the virtual env
RUN ./setup.sh && rm setup.sh requirements

COPY captain.py captain.py
COPY sailor.py sailor.py
COPY cors.py cors.py
COPY run.sh run.sh

CMD [ "./run.sh" ]