#!/bin/sh

# Copyright (C) 2015-2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

# registra no hub
#echo cat /run/secrets/dh_pwd | docker login -u ultimaratioregis --password-stdin
# registra chave do github.com
ssh -i /etc/ssh/sshcaptain -o StrictHostKeyChecking=no -o AddKeysToAgent=yes git@github.com

. env/bin/activate
gunicorn captain:app -t 120 -w 3 -b 0.0.0.0:8000