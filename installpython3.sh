#!/bin/bash

# Copyright (C) 2015-2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

echo " "
echo "installing apt packages...."
sudo apt-get -y install python3-pip libpq-dev python3-dev
echo " "
echo "upgrading pip...."
sudo pip3 install --upgrade pip