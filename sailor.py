import os

def getEnv(name):
    env = ""
    try:
        env = os.environ[name].strip()
    except KeyError:
        return None
    if len(env) > 0:
        return env
    return None

def getEnvBoolean(name):
    env = getEnv(name)
    if env is None:
        return False
    if env.lower()=="true":
        return True
    return False

def br():
    return "<br>"
def okstring():
    return " ok"
def failstring():
    return " fail"

def imagename(gitrepo,project,tag):
    rtag = ""
    if len(tag) > 0:
        rtag=':{}'.format(tag)
    return '{}/{}'.format(gitrepo,project) + rtag

def readVersion():
    version = ""
    file = open('version','r')
    version = file.read().strip()
    if len(version) > 0:
        return version
    return None