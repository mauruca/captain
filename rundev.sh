#!/bin/sh

# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

debug=0

if [ -n "$1" ]; then
    if [ "$1" == 1 ]; then
        debug=1
    else
        if [ "$1" != 0 ]; then
            echo "./rundev.sh <optionl debug|0 or 1>"
            exit 1
        fi
    fi
fi

export FLASK_DEBUG=$debug

export LC_ALL=C.UTF-8
export LANG=C.UTF-8

. env/bin/activate
export FLASK_APP=captain.py
flask run