#!/bin/sh

# Copyright (C) 2015-2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

echo " "
echo "installing virtualenv...."
pip3 install virtualenv
echo " "
echo "creating virtualenv...."
virtualenv env
echo " "
echo "starting virtualenv...."
. env/bin/activate
echo " "
echo "installing requirements...."
pip3 install --upgrade -r requirements
echo
echo
echo "to activate the environment run command bash: source env/bin/activate or sh:. env/bin/activate"