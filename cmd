echo 'secret' | docker secret create hook_secret -
echo 'ultimaratioregis' | docker secret create dh_login -
echo 'secret' | docker secret create dh_pwd -

docker run -it -v /var/run/docker.sock:/var/run/docker.sock -p 8000:8000 mauruca/cicds /bin/sh
mkdir /run/secrets
vim /run/secrets/dh_login
vim /run/secrets/dh_pwd

docker service create --detach=false --mount type=bind,source=/var/run/docker.sock,destination=/var/run/docker.sock -p 8000:8000 --secret="dh_login" --secret="dh_pwd" --secret="hook_secret" mauruca/cicds

docker stack deploy hook --compose-file file.yml