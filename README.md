# Captain Hook  
  
Hook builder for github using ssh to create simple script builds.

## Setting up the image

* Generate a ssh key 
```sh
$ certgen.sh
```
* Add the public key to github account.

* Build the image
```sh
$ docker build --rm -t mauruca/cicd .
```

## Using the image

Download following images
```sh
$ docker pull mauruca/simpleproxy && docker pull mauruca/cicd
```

Create a compose file
```yml
version: '3.1'

networks:
  buildnet:

services:

  appserver:
    image: mauruca/cicds
    networks:
      - buildnet
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    secrets:
      - dh_login
      - dh_pwd
      - hook_secret
    environment:
      CAPTAIN_DEBUG: 'true'
      CAPTAIN_UNSAFE: 'true'

  sproxy:
    image: mauruca/simpleproxy
    ports:
      - "8080:80"
    depends_on:
      - appserver
    networks:
      - buildnet

secrets:
  dh_login:
    external: true
  dh_pwd:
    external: true
  hook_secret:
    external: true
```

Define the dockerhub password as secrets. Replace <passwd> in the command below
```bash
$ echo '<secret>' | docker secret create dh_login -
$ echo '<secret>' | docker secret create dh_pwd -
$ echo '<secret>' | docker secret create hook_secret -
```

Start the services
```bash
$ docker stack deploy -c compose.yml up
```